function createCard(name, description, pictureUrl,start, end, location ) {
    return `
      <div class="shadow p-3 mb-5 bg-body-tertiary rounded">

        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location.name}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">${start}-${end}</div>

      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        console.log("response is not ok")
        
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        const columns = document.querySelectorAll('.col');
        for (let i = 0; i < data.conferences.length -1; i++) {
          let conference = data.conferences[i]
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toDateString()
            const end = new Date(details.conference.ends).toDateString()
            const location = details.conference.location
            const html = createCard(title, description, pictureUrl,start,end, location);
            // console.log(html);
          
            let column = columns[i % 3]  
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
        console.error(e);
        
      // Figure out what to do if an error is raised
    }
  
  });


















// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         console.log("response not ok")
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();
        
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

        
        
//         // get the url of the api
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         // now fetching the data response
      
//         const detailResponse = await fetch(detailUrl);
//       if (detailResponse.ok) {
//         // turn json after verified
//         const details = await detailResponse.json();
//         const descriptionTag = document.querySelector('.card-text');
//         // get the html tag that is needed
//         // set html tag to cooresponding data
//         descriptionTag.innerHTML = details.conference.description

//         const imageTag = document.querySelector('.card-img-top')
//         imageTag.src = details.conference.location.picture_url;
        
//       }

//     }
//   } catch (e) {
//     // Figure out what to do if an error is raised
//   }

// });
  